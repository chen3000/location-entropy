package wd.locationEntropy

import java.io.PrintWriter

import scala.io.Source

object IOUtils {

  def readCSV(filename: String): Iterator[GowallaRecord] = {
    val src = Source.fromFile(filename)
    src.getLines().map(parseGowallaRecord)
  }

  def parseGowallaRecord(line: String): GowallaRecord = {
    val splittedLine: Array[String] = line.split("\t")
    GowallaRecord(splittedLine.head.toLong, splittedLine(1), splittedLine(2).toDouble,
      splittedLine(3).toDouble, splittedLine(4).toLong)
  }

  def writeCSV(filename: String, data: Vector[(Long, Double, Double, Double)]): Unit = {
    val headers = Vector("LocationID", "Latitude", "Longitude", "Entropy").mkString(",")

    val csv: String = (headers +: data.sortBy(-_._4)
      .map(r => Vector(r._1.toString, r._2.toString, r._3.toString, r._4.toString).mkString(",")))
      .mkString("\n")

    new PrintWriter(filename) {
      write(csv)
      close()
    }

  }

  case class GowallaRecord(user: Long, checkInTimeString: String, latitude: Double, longitude: Double, locationId: Long)

}

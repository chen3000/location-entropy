package wd.locationEntropy

import scalaz.Scalaz
import wd.locationEntropy.IOUtils.GowallaRecord

object computeLocationEntropy {

  /** Compute the locations entropy for each location.
    *
    * Given geo-spatial coordinates :math:`L = \{ l_1, l_2, /dots ,l_{|L|} \}`,
    * each location :math:`l_i` is visited by a set of users :math:`U = \{ u_1, u_2, /dots, /u_{|U|} \}`.
    *
    * Location entropy :maht:`H(l) = -\sum_{u\in U_l} p_{l,u} log p_{l,u}`
    * :math:`u_l` the set of distinct users that visited :math:`l`
    * :math:`p_{l,u}` the fraction of visits of :math:`l` that belongs to user :math:`u`
    *
    * @param records visits
    * @return [[Vector]] of tuples ( "LocationID", "Latitude", "Longitude", "Entropy" )
    *
    */
  def apply(records: Iterator[GowallaRecord]): Vector[(Long, Double, Double, Double)] = {

    val numberOfVisitsPerLocationPerUser = computeNumberOfVisitsPerLocationPerUser(records)
    val numberOfVisitsPerLocation = computeNumberOfVisitsPerLocation(numberOfVisitsPerLocationPerUser).toMap

    val log2 = (x: Double) => scala.math.log10(x) / scala.math.log10(2.0)

    numberOfVisitsPerLocationPerUser.map(r => {
      val locationId: Long = r._1._1
      val userId: Long = r._1._2
      val numberOfVisitsForThisUser: Int = r._2
      val totalNumberOfVisits = numberOfVisitsPerLocation(locationId)
      val lat: Double = r._1._3
      val long: Double = r._1._4

      val p = numberOfVisitsForThisUser.toDouble / totalNumberOfVisits

      (locationId, userId, lat, long, -p * log2(p))

    }).groupBy(_._1).mapValues(r => (r.head._1, r.head._3, r.head._4, r.map(_._5).sum)).values.toVector

  }

  def computeNumberOfVisitsPerLocationPerUser(records: Iterator[GowallaRecord]): Vector[((Long, Long, Double, Double), Int)] = {
    import Scalaz._
    records.map(r => Map((r.locationId, r.user, r.latitude, r.longitude) -> 1)).reduce(_ |+| _).toVector
  }

  def computeNumberOfVisitsPerLocation(visitsCount: Vector[((Long, Long, Double, Double), Int)]): Vector[(Long, Int)] =
    visitsCount.map(r => (r._1._1, r._2)).groupBy(_._1)
      .mapValues(_.map(_._2).sum).toVector

}

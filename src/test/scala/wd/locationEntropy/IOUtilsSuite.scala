package wd.locationEntropy

import org.scalatest.{FlatSpec, Matchers}
import wd.locationEntropy.IOUtils._

class IOUtilsSuite extends FlatSpec with Matchers {

  "parseGowallaRecord" should "parse record in Gowalla dataset" in {

    parseGowallaRecord("0\t2010-10-19T23:55:27Z\t30.2359091167\t-97.7951395833\t22847") should be(GowallaRecord(0, "2010-10-19T23:55:27Z", 30.2359091167, -97.7951395833, 22847))

  }

}

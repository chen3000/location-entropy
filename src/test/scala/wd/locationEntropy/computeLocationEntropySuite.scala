package wd.locationEntropy

import org.scalatest.{FlatSpec, Matchers}
import wd.locationEntropy.IOUtils.GowallaRecord
import wd.locationEntropy.computeLocationEntropy._


class computeLocationEntropySuite extends FlatSpec with Matchers {

  val testInputData = Vector(
    GowallaRecord(0, "2010-10-19T23:55:27Z", 30, -97, 0),
    GowallaRecord(1, "2010-10-19T23:55:27Z", 30, -97, 0),
    GowallaRecord(1, "2010-10-19T23:55:27Z", 30, -97, 0),
    GowallaRecord(2, "2010-10-19T23:55:27Z", 30, -97, 1),
    GowallaRecord(0, "2010-10-19T23:55:27Z", 30, -97, 0),
    GowallaRecord(0, "2010-10-19T23:55:27Z", 30, -97, 1)
  )

  "computeNumberOfVisitsPerLocationPerUser" should "compute the number of visits per user per location." in {

    computeNumberOfVisitsPerLocationPerUser(testInputData.toIterator) should be(
      Vector(((0, 1, 30.0, -97.0), 2), ((0, 0, 30.0, -97.0), 2), ((1, 2, 30.0, -97.0), 1), ((1, 0, 30.0, -97.0), 1))
    )

  }

  "computeNumberOfVisitsPerLocation" should "compute the number of visits per location." in {

    computeNumberOfVisitsPerLocation(computeNumberOfVisitsPerLocationPerUser(testInputData.toIterator)) should be(
      Vector((1, 2), (0, 4))
    )
  }

  "computeLocationEntropy" should "compute the location entropy for each location." in {

    computeLocationEntropy(testInputData.toIterator) should be(
      Vector((1, 1.0), (0, 1.0))
    )

  }

}

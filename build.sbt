name := "location-entropy"

version := "0.1"

scalaVersion := "2.11.11"

// https://mvnrepository.com/artifact/org.rogach/scallop_2.11
lazy val scallop = "org.rogach" % "scallop_2.11" % "3.1.0"

// https://mvnrepository.com/artifact/org.scalaz/scalaz-core
lazy val scalaz = "org.scalaz" %% "scalaz-core" % "7.2.26"

// https://mvnrepository.com/artifact/org.scalatest/scalatest
lazy val scalatest = "org.scalatest" %% "scalatest" % "3.0.5" % Test

libraryDependencies += scallop
libraryDependencies += scalaz
libraryDependencies += scalatest

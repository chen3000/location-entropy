# Dataset

We are using the public dataset `http://snap.stanford.edu/data/loc-gowalla_totalCheckins.txt.gz`
Due to the size of the data, only first 100k records are included in this repository.

This application is tested again the whole dataset.
The findings are generated using the whole dataset.

# Findings (QGIS)

## Entropy Map for locations with entropy >= 1.0

![picture](pngs/entropy-map.png)

## Entropy Map for locations with entropy >= 1.0 within Singapore

![picture](pngs/singapore-location-entropy.png)

## Number of locations with entropy >= 1.0 for Singapore sub-zones

![picture](pngs/num_of_points.png)
